
#include <GL\glew.h>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include <cstdio>
#include <cassert>
#include <iostream>
#include "GL_framework.h"


bool e1 = false;
bool e2 = false;
bool e3 = false;
bool e4 = false;

bool cambio = false;
double tini;

int w;
int h;

namespace ImGui {
	void Render();
}

namespace Cube {
	void setupCube();
	void cleanupCube();
	void updateCube(const glm::mat4& transform);
	void drawCube();
	void draw2Cubes(double curentTime);
}

namespace RenderVars {
	const float FOV = glm::radians(65.f);
	const float zNear = 1.f;
	const float zFar = 50.f;

	glm::mat4 _projection;
	glm::mat4 _modelView;
	glm::mat4 _MVP;
	glm::mat4 _inv_modelview;
	glm::vec4 _cameraPoint;

	struct prevMouse {
		float lastx, lasty;
		MouseEvent::Button button = MouseEvent::Button::None;
		bool waspressed = false;
	} prevMouse;

	float panv[3] = { 0.f, -5.f, -15.f };
	float rota[2] = { 0.f, 0.f };
}
namespace RV = RenderVars;

void GLinit(int width, int height) {
	glViewport(0, 0, width, height);
	glClearColor(0.2f, 0.2f, 0.2f, 1.f);
	glClearDepth(1.f);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	w = width;
	h = height;

	//RV::_projection = glm::perspective(RV::FOV, (float)width / (float)height, RV::zNear, RV::zFar);
	float scale = 50.0f;
	//std::cout << -(float)width / scale << " / " << (float)width / scale << " / " << -(float)height / scale << " / " << (float)height / scale << " / " << RV::zNear << " / " << RV::zFar << std::endl;
	//RV::_projection = glm::ortho(-10.0f, 10.0f, -5.0f, 5.0f, RV::zNear, RV::zFar);
	// Setup shaders & geometry

	Cube::setupCube();


}

void GLcleanup() {

	Cube::cleanupCube();

}

void myRenderCode(double currentTime) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	RV::_modelView = glm::mat4(1.f);
	RV::_modelView = glm::translate(RV::_modelView, glm::vec3(0.f, 0.f, -20.f));
	//RV::_modelView = glm::rotate(RV::_modelView, RV::rota[1], glm::vec3(1.f, 0.f, 0.f));
	//RV::_modelView = glm::rotate(RV::_modelView, RV::rota[0], glm::vec3(0.f, 1.f, 0.f));
	
	//std::cout << "E1: " << e1 << ", E2: " << e2 << ", E3: " << e3 << ", E4: " << e4 << std::endl;

	if(e1)
	{
		///TRAVELING LATERAL DE CAMARA EN ORTONORMAL
		if (cambio)
		{
			RV::_modelView = glm::translate(RV::_modelView, glm::vec3(0.75f, 0.f, -20.f));
			cambio = false;
			tini = currentTime;
		}
		RV::_modelView = glm::translate(RV::_modelView, glm::vec3(0.75f*(currentTime-tini), 0.f, -20.f));
		RV::_projection = glm::ortho(-10.0f, 10.0f, -5.0f, 5.0f, RV::zNear, RV::zFar);
		RV::_modelView = glm::rotate(RV::_modelView, 45.f, glm::vec3(1.f, 0.f, 0.f));
		RV::_modelView = glm::rotate(RV::_modelView, 45.f, glm::vec3(0.f, 1.f, 0.f));

	}
	if(e2)
	{
		///ZOOM EN PROYECCION
		if (cambio)
		{
			RV::_modelView = glm::translate(RV::_modelView, glm::vec3(0.f, 0.f, 1.f));
			cambio = false;
			tini = currentTime;
		}
		RV::_modelView = glm::translate(RV::_modelView, glm::vec3(0.f, 0.f, 1.f*(currentTime-tini)));
		RV::_projection = glm::perspective(RV::FOV, (float)w / (float)h, RV::zNear, RV::zFar);

	}
	if(e3)
	{
		///AMPLIAR FIELD OF VIEW EN PROYECCION
		if (cambio)
		{
			RV::_modelView = glm::translate(RV::_modelView, glm::vec3(0.f, 0.f, 0.f));
			RV::_projection = glm::perspective(glm::radians(40.f), (float)w / (float)h, RV::zNear, RV::zFar);
			cambio = false;
			tini = currentTime;
		}
		RV::_modelView = glm::translate(RV::_modelView, glm::vec3(0.f, 0.f, 0.f));
		RV::_projection = glm::perspective(glm::radians(40.f+5*(float)(currentTime-tini)) , (float)w / (float)h, RV::zNear, RV::zFar);
	}
	if (e4)
	{
		///DOLLY EFFECT (mezclar el e2 y el e3)
		if (cambio)
		{
			//RV::_modelView = glm::translate(RV::_modelView, glm::vec3(0.f, 0.f, 1.f));
			RV::_modelView = glm::translate(RV::_modelView, glm::vec3(0.f, 0.f, 0.f));
			RV::_projection = glm::perspective(glm::radians(40.f), (float)w / (float)h, RV::zNear, RV::zFar);
			cambio = false;
			tini = currentTime;
		}
		RV::_modelView = glm::translate(RV::_modelView, glm::vec3(0.f, 0.f, 1.0f*(currentTime - tini)));
		RV::_projection = glm::perspective(RV::FOV, (float)w / (float)h, RV::zNear, RV::zFar);
		RV::_modelView = glm::translate(RV::_modelView, glm::vec3(0.f, 0.f, 0.f));
		RV::_projection = glm::perspective(glm::radians(40.f + 4.25f * (float)(currentTime - tini)), (float)w / (float)h, RV::zNear, RV::zFar);
	}



	RV::_MVP = RV::_projection * RV::_modelView;



	// render code
	Cube::draw2Cubes(currentTime);



	ImGui::Render();
}


//////////////////////////////////// COMPILE AND LINK
GLuint compileShader(const char* shaderStr, GLenum shaderType, const char* name = "") {
	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &shaderStr, NULL);
	glCompileShader(shader);
	GLint res;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &res);
	if (res == GL_FALSE) {
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &res);
		char *buff = new char[res];
		glGetShaderInfoLog(shader, res, &res, buff);
		fprintf(stderr, "Error Shader %s: %s", name, buff);
		delete[] buff;
		glDeleteShader(shader);
		return 0;
	}
	return shader;
}

void linkProgram(GLuint program) {
	glLinkProgram(program);
	GLint res;
	glGetProgramiv(program, GL_LINK_STATUS, &res);
	if (res == GL_FALSE) {
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &res);
		char *buff = new char[res];
		glGetProgramInfoLog(program, res, &res, buff);
		fprintf(stderr, "Error Link: %s", buff);
		delete[] buff;
	}
}

namespace Cube {
	GLuint cubeVao;
	GLuint cubeVbo[3];
	GLuint cubeShaders[2];
	GLuint cubeProgram;
	glm::mat4 objMat = glm::mat4(1.f);

	extern const float halfW = 0.5f;
	int numVerts = 24 + 6; // 4 vertex/face * 6 faces + 6 PRIMITIVE RESTART

						   //   4---------7
						   //  /|        /|
						   // / |       / |
						   //5---------6  |
						   //|  0------|--3
						   //| /       | /
						   //|/        |/
						   //1---------2
	glm::vec3 verts[] = {
		glm::vec3(-halfW, -halfW, -halfW),
		glm::vec3(-halfW, -halfW,  halfW),
		glm::vec3(halfW, -halfW,  halfW),
		glm::vec3(halfW, -halfW, -halfW),
		glm::vec3(-halfW,  halfW, -halfW),
		glm::vec3(-halfW,  halfW,  halfW),
		glm::vec3(halfW,  halfW,  halfW),
		glm::vec3(halfW,  halfW, -halfW)
	};
	glm::vec3 norms[] = {
		glm::vec3(0.f, -1.f,  0.f),
		glm::vec3(0.f,  1.f,  0.f),
		glm::vec3(-1.f,  0.f,  0.f),
		glm::vec3(1.f,  0.f,  0.f),
		glm::vec3(0.f,  0.f, -1.f),
		glm::vec3(0.f,  0.f,  1.f)
	};

	glm::vec3 cubeVerts[] = {
		verts[1], verts[0], verts[2], verts[3],
		verts[5], verts[6], verts[4], verts[7],
		verts[1], verts[5], verts[0], verts[4],
		verts[2], verts[3], verts[6], verts[7],
		verts[0], verts[4], verts[3], verts[7],
		verts[1], verts[2], verts[5], verts[6]
	};
	glm::vec3 cubeNorms[] = {
		norms[0], norms[0], norms[0], norms[0],
		norms[1], norms[1], norms[1], norms[1],
		norms[2], norms[2], norms[2], norms[2],
		norms[3], norms[3], norms[3], norms[3],
		norms[4], norms[4], norms[4], norms[4],
		norms[5], norms[5], norms[5], norms[5]
	};
	GLubyte cubeIdx[] = {
		0, 1, 2, 3, UCHAR_MAX,
		4, 5, 6, 7, UCHAR_MAX,
		8, 9, 10, 11, UCHAR_MAX,
		12, 13, 14, 15, UCHAR_MAX,
		16, 17, 18, 19, UCHAR_MAX,
		20, 21, 22, 23, UCHAR_MAX
	};




	const char* cube_vertShader =
		"#version 330\n\
	in vec3 in_Position;\n\
	in vec3 in_Normal;\n\
	out vec4 vert_Normal;\n\
	uniform mat4 objMat;\n\
	uniform mat4 mv_Mat;\n\
	uniform mat4 mvpMat;\n\
	void main() {\n\
		gl_Position = mvpMat * objMat * vec4(in_Position, 1.0);\n\
		vert_Normal = mv_Mat * objMat * vec4(in_Normal, 0.0);\n\
	}";


	const char* cube_fragShader =
		"#version 330\n\
in vec4 vert_Normal;\n\
out vec4 out_Color;\n\
uniform mat4 mv_Mat;\n\
uniform vec4 color;\n\
void main() {\n\
	out_Color = vec4(color.xyz * dot(vert_Normal, mv_Mat*vec4(0.0, 1.0, 0.0, 0.0)) + color.xyz * 0.3, 1.0 );\n\
}";
	void setupCube() {
		glGenVertexArrays(1, &cubeVao);
		glBindVertexArray(cubeVao);
		glGenBuffers(3, cubeVbo);

		glBindBuffer(GL_ARRAY_BUFFER, cubeVbo[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVerts), cubeVerts, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, cubeVbo[1]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNorms), cubeNorms, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);

		glPrimitiveRestartIndex(UCHAR_MAX);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeVbo[2]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeIdx), cubeIdx, GL_STATIC_DRAW);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		cubeShaders[0] = compileShader(cube_vertShader, GL_VERTEX_SHADER, "cubeVert");
		cubeShaders[1] = compileShader(cube_fragShader, GL_FRAGMENT_SHADER, "cubeFrag");

		cubeProgram = glCreateProgram();
		glAttachShader(cubeProgram, cubeShaders[0]);
		glAttachShader(cubeProgram, cubeShaders[1]);
		glBindAttribLocation(cubeProgram, 0, "in_Position");
		glBindAttribLocation(cubeProgram, 1, "in_Normal");
		linkProgram(cubeProgram);
	}
	void cleanupCube() {
		glDeleteBuffers(3, cubeVbo);
		glDeleteVertexArrays(1, &cubeVao);

		glDeleteProgram(cubeProgram);
		glDeleteShader(cubeShaders[0]);
		glDeleteShader(cubeShaders[1]);
	}
	void updateCube(const glm::mat4& transform) {
		objMat = transform;
	}
	void drawCube() {
		glEnable(GL_PRIMITIVE_RESTART);
		glBindVertexArray(cubeVao);
		glUseProgram(cubeProgram);
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.1f, 1.f, 1.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		glUseProgram(0);
		glBindVertexArray(0);
		glDisable(GL_PRIMITIVE_RESTART);
	}

	void draw2Cubes(double currentTime) {
		glEnable(GL_PRIMITIVE_RESTART);
		glBindVertexArray(cubeVao);
		glUseProgram(cubeProgram);
		//glm::mat4 t = glm::rotate(glm::mat4(), (float)(5.0f*currentTime), glm::vec3(0, 1, 0));
		//t*= glm::translate(glm::mat4(), glm::vec3(1.f, 0.f, 0.f));
		glm::mat4 t = glm::translate(glm::mat4(), glm::vec3(0.f, 0.f, 0.f));

		objMat = t;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.1f, 1.f, 1.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		/*t = glm::translate(glm::mat4(), glm::vec3(2.f, 2.f, 0.f));
		objMat = t;
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.5f,0.5*cosf(currentTime), 0.5*sinf(currentTime), 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);*/

		/*t = glm::scale(glm::mat4(), glm::vec3(1+(0.5f+0.5f*sinf(currentTime)), 1+(0.5f + 0.5f*sinf(currentTime)), 1+(0.5f + 0.5f*sinf(currentTime))));
		t *= glm::rotate(glm::mat4(), (float)(2.0f*currentTime), glm::vec3(0, 1, 0));
		t *= glm::translate(glm::mat4(), glm::vec3(3.0f, 4 + 2 * sinf(currentTime), 0.0f));*/

		glm::mat4 m;

		glm::mat4 s= glm::scale(glm::mat4(), glm::vec3(2.5f, 2.5f, 2.5f));
		glm::mat4 t1 = glm::translate(glm::mat4(), glm::vec3(-2.5f, 0.5f, -4.0f));
		//glm::mat4 r1= glm::rotate(glm::mat4(), (float)(2.0f*currentTime), glm::vec3(0, 1, 0));
		//glm::mat4 t2= glm::translate(glm::mat4(), glm::vec3(3.0f, 0.0f, 0.0f));
		//glm::mat4 r2= glm::rotate(glm::mat4(), (float)(2.0f*currentTime), glm::vec3(0, -1, 0));

		/*t = glm::translate(glm::mat4(), glm::vec3(3.0f, 0.0f, 0.0f));
		t *= glm::scale(glm::mat4(), glm::vec3(1 + (0.5f + 0.5f*sinf(currentTime)), 1 + (0.5f + 0.5f*sinf(currentTime)), 1 + (0.5f + 0.5f*sinf(currentTime))));
		t *= glm::rotate(glm::mat4(), (float)(2.0f*currentTime), glm::vec3(0, 1, 0));
		//t *= glm::translate(glm::mat4(), glm::vec3(3.0f, 4 + 2 * sinf(currentTime), 0.0f));
		t *= glm::translate(glm::mat4(), glm::vec3(3.0f, 0.0f, 0.0f));
		t *= glm::rotate(glm::mat4(), (float)(2.0f*currentTime), glm::vec3(0, -1, 0));*/

		//m = r2*t2*r1*t1*s;
		m = t1*s;
		objMat = m;



		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.5f, 0.5*cosf(currentTime), 0.5*sinf(currentTime), 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		glUseProgram(0);
		glBindVertexArray(0);
		glDisable(GL_PRIMITIVE_RESTART);
	}


}